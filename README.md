# Prediksi Harga Pangan Indonesia dengan Metode ANFIS

## Data Penelitian
Penelitian ini menggunakan data harga pangan khususnya beras yang bersumber dari situs https://www.bps.go.id/indicator/20/295/12/rata-rata-harga-beras-di-tingkat-perdagangan-besar-grosir-indonesia.html tahun 2010 - 2020 setiap bulannya sebanyak 132 data. Data berisi 12 kolom (bulan Januari - Desember) dan 11 baris (tahun 2010 - 2020). Kemudian data tersebut digunakan sebagai data latih 80% yaitu 108 data dan data pengujiannya 20% yaitu 24 data. 


## Anfis
ANFIS (Adaptive Neuro-Fuzzy Inference System) dengan jumlah fungsi keanggotaan 2, jumlah iterasi (epoch) 300, dan tipe fungsi keanggotaan gaussmf sebagai variabel input agar mendapatkan nilai error terkecil. Setelah dilakukan pelatihan dan pengujian, didapatkan nilai MSE dan MAPE pengujian berturut-turut adalah  0.9176 dan 0.70059%.
