clc; clear; close all; warning off all;
 
% input data
data = xlsread('DataBeras',1,'B2:M13');
 
% Proses Normalisasi Data
max_data = max(max(data));
min_data = min(min(data));
 
[m,n] = size(data);
data_norm = zeros(m,n); %membentuk normalisasi data
for x = 1:m
    for y = 1:n
        data_norm(x,y) = (data(x,y)-min_data)/(max_data-min_data); %rumus normalisasi data
    end
end

% Training
% Menyiapkan data training
data_norm = data_norm';
tahun_latih = 11; % 2010 - 2020
bulan_latih = 6; % Prediksi harga dihitung dari data 6 bulan sebelumnya
jumlah_bulan = 12; % januari - desember
data_latih = zeros(jumlah_bulan*tahun_latih-bulan_latih,bulan_latih); %membuat data latih
 
for m = 1:jumlah_bulan*tahun_latih-bulan_latih
    for n = 1:bulan_latih
        data_latih(m,n) = data_norm(m+n-1);
    end
end
 
% Target training
target_latih = zeros(jumlah_bulan*tahun_latih-bulan_latih,1);
for m = 1:jumlah_bulan*tahun_latih-bulan_latih
    target_latih(m) = data_norm(bulan_latih+m);
end

% Proses Training
trnData = [data_latih,target_latih];
numMFs = 2; % Nilai Fungsi Keanggotaan
mfType = 'gaussmf'; % Tipe Fungsi Keanggotaan
epoch_n = 300; % Jumlah Iterasi
in_fis = genfis1(trnData,numMFs,mfType); %rumus ANFIS
out_fis = anfis(trnData,in_fis,epoch_n); %rumus ANFIS
 
out_pelatihan = evalfis(data_latih,out_fis);
mse_pelatihan = mse(target_latih,data_latih); %menghitung mse latih
diff = out_pelatihan-target_latih;
mape_pelatihan = (100/numel(out_pelatihan))*(sum(sum(abs(diff)/target_latih))); %menghitung MAPE pelatihan
 
% Denormalisasi hasil training
out_pelatihan_asli = (out_pelatihan*(max_data-min_data))+min_data;

% Target latih asli
target_latih_asli = zeros(jumlah_bulan*tahun_latih-bulan_latih,1);
data = data';
for m = 1:jumlah_bulan*tahun_latih-bulan_latih
    target_latih_asli(m) = data(bulan_latih+m); 
end
 
% Menampilkan hasil Training dalam Grafik 
figure
plot(target_latih_asli,'o-','LineWidth',2,'MarkerSize',4)
hold on
grid on
plot(out_pelatihan_asli,'o-','LineWidth',2,'MarkerSize',4)
hold off
title('Training Result')
xlabel('Month')
ylabel('Price')
legend('Target','Output')

% Data Testing
tahun_uji = 1; % 2021
bulan_uji = 6; % Harga diprediksi berdasarkan 6 bulan sebelumnya
jumlah_bulan = 12; % januari - desember
 
data_uji = zeros(jumlah_bulan*tahun_uji,bulan_uji); % Pembentukan data uji
 
for m = 1:jumlah_bulan*tahun_uji
    for n = 1:bulan_uji
        data_uji(m,n) = data_norm(jumlah_bulan*tahun_latih-bulan_latih+m+n-1); %Menghitung data uji
    end
end
  
% Target Test
target_uji = zeros(jumlah_bulan*tahun_uji,1);
for m = 1:jumlah_bulan*tahun_uji
    target_uji(m) = data_norm(jumlah_bulan*tahun_latih+m);
end

% Proses Testing
out_pengujian = evalfis(data_uji,out_fis);
mse_pengujian = mse(target_uji,data_uji); %menghitung mse uji
diff = out_pengujian-target_uji;
mape_pengujian = (100/numel(out_pengujian))*(sum(sum(abs(diff)/target_uji))); %Menghitung MAPE uji
 
% Denormalisasi hasil test
out_pengujian_asli = (out_pengujian*(max_data-min_data))+min_data;
 
% Grafik Hasil Prediksi
figure
hold on
grid on
plot(out_pengujian_asli,'o-','LineWidth',2,'MarkerSize',4)
hold off
title('Prediction Result in 2021')
xlabel('Month')
ylabel('Price')
legend('Prediction')
